﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace D2ResourceEditor
{
    public partial class Form1 : Form
    {
        public class InfoEntry
        {
            public string context;
            public string id;
            public Object item;
        }

        public class SourceInfo
        {
            public string name;
            public bool uppercase = true;
            public string source;
        }

        List<string> specialCats = new List<string>() { "L_LEADER", "L_SUMMON", "L_ILLUSION" };
        List<SourceInfo> infos = new List<SourceInfo>();
        List<InfoEntry> searchResults = new List<InfoEntry>();
        List<InfoEntry> totalResults = new List<InfoEntry>();
        GameDataModel gameModel = new GameDataModel();
        SettingsManager settings = new SettingsManager("settings.xml");
        List<string> log = new List<string>();
        string currentId = "";
        
        Dictionary<string, GameResource> resources = new Dictionary<string, GameResource>();
        bool loadingState = false;

        private void Log(string str)
        {
            log.Add(str + "\n");
        }

        Timer timer1 = new Timer();
        List<Image> animImages = new List<Image>();
        int animIndex = 0;
        public Form1()
        {
            InitializeComponent();
            resources.Add("/Imgs/Faces.ff", new GameResource());
            infos.Add(new SourceInfo() { source = "/Imgs/Faces.ff", name = "FACE.PNG" });
            infos.Add(new SourceInfo() { source = "/Imgs/Faces.ff", name = "FACEB.PNG" });

            resources.Add("/Imgs/Events.ff", new GameResource());
            infos.Add(new SourceInfo() { source = "/Imgs/Events.ff", name = "" });

            resources.Add("/Imgs/IsoUnit.ff", new GameResource());
            for (int i = 0; i < 8; ++i)
            {
                infos.Add(new SourceInfo() { source = "/Imgs/IsoUnit.ff", name = "MOVE" + i.ToString() });
                infos.Add(new SourceInfo() { source = "/Imgs/IsoUnit.ff", name = "SMOV" + i.ToString() });
            }
            for (int i = 0; i < 8; ++i)
            {
                infos.Add(new SourceInfo() { source = "/Imgs/IsoUnit.ff", name = "STOP" + i.ToString() });
                infos.Add(new SourceInfo() { source = "/Imgs/IsoUnit.ff", name = "SSTO" + i.ToString() });
            }

            resources.Add("/Imgs/BatUnits.ff", new GameResource()); 
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HEFFA1B00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HHITA1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HHITA1D00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HHITS1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HHITS1D00" });

            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HMOVA1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HMOVA1D00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HMOVA2A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HMOVA2D00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HMOVS1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "HMOVS1D00" });

            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "IDLEA1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "IDLEA1D00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "IDLES1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "IDLES1D00" });

            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "STILA1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "STILA1D00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "STILS1A00" });
            infos.Add(new SourceInfo() { source = "/Imgs/BatUnits.ff", uppercase = true, name = "STILS1D00" });

            pathTextBox.Text = settings.Value("GamePath", "");
            InitializeTimer();
        }
        private void InitializeTimer()
        { 
            timer1.Interval = 40;
            timer1.Tick += new EventHandler(Timer1_Tick); 
            timer1.Enabled = true;

        }

        private void Timer1_Tick(object Sender, EventArgs e)
        {
            if (animImages.Count < 1 || animIndex >= animImages.Count)
                return;
            if (focusCheckBox.Checked)
                pictureBox1.Image = ImageHelper.CropImage(animImages[animIndex++], new Rectangle(230, 230, 400, 400));
            else
                pictureBox1.Image = animImages[animIndex++];
            if (animIndex >= animImages.Count)
                animIndex = 0;
        }

        private void selectPathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDialog = new FolderBrowserDialog();
            if (browserDialog.ShowDialog() == DialogResult.Cancel)
                return;

            pathTextBox.Text = browserDialog.SelectedPath;
            parseGameButton_Click(sender, e);
        }

        private void parseGameButton_Click(object sender, EventArgs e)
        {
            log.Clear();
            if (!System.IO.File.Exists(pathTextBox.Text + "\\globals\\Tglobal.dbf"))
            {
                Log("Invalid path! File " + pathTextBox.Text + "\\globals\\Tglobal.dbf not found!!!");
                return;
            }
            gameModel.Init(pathTextBox.Text, ref settings);
            settings.SetValue("GamePath", pathTextBox.Text);
            foreach(string key in resources.Keys)
            {
                resources[key].Read(pathTextBox.Text + key);
            }
            
            LoadUnits();
            textBox1.Text = "5029";
        }

        private void LoadUnits()
        {
            loadingState = true;
            foreach (GameUnit unit in gameModel._Units.Values)
            {
                string context = unit.name;
                if (specialCats.Contains(unit.category))
                    context += " [" + TranslationHelper.Instance().Tr(unit.category) + "]";
                totalResults.Add(new InfoEntry()
                {
                    context = context,
                    id = unit.objId,
                    item = unit
                });
            }
            totalResults.Sort((x, y) => String.Compare(x.context, y.context));
            loadingState = false;
            textBox1_TextChanged(null, null);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (loadingState)
                return;
            UpdateFilteredList(true);
        }

        void UpdateFilteredList(bool select)
        {
            string filter = textBox1.Text;
            searchResults.Clear();
            listBox1.Items.Clear();
            foreach (InfoEntry res in totalResults)
            {
                if (filter == "" ||
                    res.context.ToLower().Contains(filter.ToLower())
                    || res.id.ToLower().Contains(filter.ToLower())
                    )
                {
                    searchResults.Add(res);
                }
            }
            loadingState = true;
            foreach (InfoEntry res in searchResults)
            {
                listBox1.Items.Add(res.context);
            }
            loadingState = false;
            if (select && listBox1.Items.Count > 0)
            {
                listBox1.SelectedIndex = 0;
                listBox1_SelectedIndexChanged(null, null);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loadingState)
                return;
            listBox2.Items.Clear();
            int index = listBox1.SelectedIndex;
            if (index < 0 || index > searchResults.Count)
                return;
            currentId = searchResults[index].id;
            loadingState = true;
            for (int i = 0; i < infos.Count; ++i)
            {
                string name = currentId;
                if (infos[i].uppercase)
                    name = name.ToUpper();
                name += infos[i].name;
                int framesCount = resources[infos[i].source].GetFramesCountById(name);
                listBox2.Items.Add(infos[i].source + "    " + name + "    [" + framesCount + "]");
            }
            loadingState = false;
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loadingState)
                return;
            int index = listBox2.SelectedIndex;
            if (index < 0 || index > infos.Count)
                return;
            string name = currentId;
            if (infos[index].uppercase)
                name = name.ToUpper();
            name += infos[index].name;
            animIndex = 0;
            animImages = resources[infos[index].source].GetFramesById_(name);
            if (animImages.Count < 1)
                pictureBox1.Image = null;
        }

        private void replaceButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "gifs|*.gif;";
            openDialog.InitialDirectory = settings.Value("openPath", ".");
            if (openDialog.ShowDialog() != DialogResult.OK)
                return;

            Image gifImg = Image.FromFile(openDialog.FileName);
            FrameDimension dimension = new FrameDimension(gifImg.FrameDimensionsList[0]);
            // Number of frames
            int frameCount = gifImg.GetFrameCount(dimension);
            // Return an Image at a certain index
            animImages.Clear();
            for (int i = 0; i < frameCount; ++i)
            {
                gifImg.SelectActiveFrame(dimension, i);
                Image frame = new Bitmap(gifImg.Width, gifImg.Height);
                using (Graphics g = Graphics.FromImage(frame))
                {
                    g.DrawImage(gifImg, new Rectangle(0, 0, gifImg.Width, gifImg.Height));
                }
                animImages.Add(frame);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {

        }
    }
}
