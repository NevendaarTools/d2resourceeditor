﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace D2ResourceEditor
{
    public class ImageHelper
    {
        public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }
        public static Bitmap CropImage(Bitmap source, Rectangle section)
        {
            var bitmap = new Bitmap(section.Width, section.Height);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel);
                return bitmap;
            }
        }

        public static Bitmap CropImage(Image source, Rectangle section)
        {
            var bitmap = new Bitmap(section.Width, section.Height);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel);
                return bitmap;
            }
        }


        public static Image MakeTransparent(ref Image image)
        {
            if (image.Palette.Entries.Count() > 0)
            {
                ColorPalette palette = image.Palette;
                Color[] entries = palette.Entries;
                entries[0] = Color.Transparent;
                for (int i = 1; i < entries.Count(); ++i)
                {
                    if (entries[i].R > 254 && entries[i].B > 254)
                        entries[i] = Color.FromArgb(0, entries[i].R, entries[i].G, entries[i].B);
                }
                image.Palette = palette;
                return image;
            }
            else
            {
                Bitmap processedBitmap = new Bitmap(image);
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

                int bytesPerPixel = Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int byteCount = bitmapData.Stride * processedBitmap.Height;
                byte[] pixels = new byte[byteCount];
                IntPtr ptrFirstPixel = bitmapData.Scan0;
                Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;

                for (int y = 0; y < heightInPixels; y++)
                {
                    int currentLine = y * bitmapData.Stride;
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        byte oldBlue = pixels[currentLine + x];
                        byte oldGreen = pixels[currentLine + x + 1];
                        byte oldRed = pixels[currentLine + x + 2];
                        byte oldAlpha = 255;
                        if (bytesPerPixel == 4)
                            oldAlpha = pixels[currentLine + x + 3];

                        pixels[currentLine + x + 0] = oldBlue;
                        pixels[currentLine + x + 1] = oldGreen;
                        pixels[currentLine + x + 2] = oldRed;

                        if (oldRed > 254 && oldBlue > 254)
                        {
                            if (oldGreen < 1 || oldAlpha < 5)
                                pixels[currentLine + x + 3] = 0;
                            else
                            {
                                int g = 0; g++;
                            }
                        }
                    }
                }

                // copy modified bytes back
                Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);
                processedBitmap.UnlockBits(bitmapData);
                return processedBitmap;
            }
        }
    }

}
